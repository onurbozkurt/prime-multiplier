require './lib/prime'

class Table
  attr_reader :size, :rows

  def initialize(size = 10)
    @size = size
    @list = Prime.first(@size)
    @rows = []
    calculate_rows
  end

  def header
    @list.insert(0, '')
  end

  private

  def calculate_rows
    @list.each_with_index do |num, index|
      @rows << @list.map{|val| val * num}.insert(0, num)
    end
  end

end
