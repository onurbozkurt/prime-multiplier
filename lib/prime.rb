class Prime

  def self.is_prime?(number)
    return false if number < 2
    (2..Math.sqrt(number).floor).each {|n| return false if (number % n) == 0 }

    true
  end

  def self.list(max)
    nums = (0..max).to_a
    nums[0] = nums[1] = nil

    nums.each do |num|
      next if num.nil?
      break if (num ** 2) > max
      (num ** 2).step(max, num) { |n| nums[n] = nil }
    end

    nums.compact
  end

  def self.first(count)
    prime_to_natural_ratio = 20
    list(count * prime_to_natural_ratio).first(count)
  end

end
