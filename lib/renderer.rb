class Renderer

  def initialize(table)
    @table = table
  end

  def rjust_value
    rjust_value = @table.rows.last.last.to_s.length
  end

  def render
    puts @table.header.map{|c| c.to_s.rjust(rjust_value, ' ') }.join('|')
    @table.rows.each do |row|
      puts row.map{|c| c.to_s.rjust(rjust_value, ' ') }.join('|')
    end
  end

end
