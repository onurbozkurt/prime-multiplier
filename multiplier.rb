#!/usr/bin/env ruby
require './lib/table'
require './lib/renderer'

count = (ARGV[0] || 10).to_i
table = Table.new(count)
renderer = Renderer.new(table)
renderer.render
