require './lib/table'

RSpec.describe Table do
  let(:table) { Table.new }

  it 'should have a size property which can be assigned on initialization' do
    table = Table.new(20)
    expect(table.size).to eq(20)
  end

  it 'should have a default size of 10' do
    expect(table.size).to eq(10)
  end

  context '#header' do
    it 'should have the initial cell empty' do
      expect(table.header.first).to eq('')
    end

    it 'should list prime numbers as header' do
      expect(table.header).to eq(['', 2, 3, 5, 7, 11, 13, 17, 19, 23, 29])
    end
  end

  context '#rows' do
    it 'should have rows count equal to size' do
      expect(table.rows.size).to eq(table.size)
    end

    it 'should have first element of each row equal to prime list' do
      expect(table.rows.map(&:first)).to eq([2, 3, 5, 7, 11, 13, 17, 19, 23, 29])
    end

    it 'should have first row with correct values' do
      expect(table.rows.first).to eq([2, 4, 6, 10, 14, 22, 26, 34, 38, 46, 58])
    end

    it 'should have last row with correct values' do
      expect(table.rows.last).to eq([29, 58, 87, 145, 203, 319, 377, 493, 551, 667, 841])
    end
  end

end
