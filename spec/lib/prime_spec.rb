require './lib/prime'

RSpec.describe Prime do
  let(:primes) { [2, 3, 5, 7, 11, 13, 17, 19, 23, 29] }
  let(:non_primes) { [0, 1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18] }

  context '#is_prime?' do

    it 'should return true for primes' do
      primes.each do |num|
        expect(Prime.is_prime?(num)).to be_truthy
      end
    end

    it 'should return false for non primes' do
      non_primes.each do |num|
        expect(Prime.is_prime?(num)).to be_falsey
      end
    end

  end

  context '#list' do
    it 'should list primes' do
      expect(Prime.list(1)).to eq([])
      expect(Prime.list(2)).to eq([2])
      expect(Prime.list(3)).to eq([2, 3])
      expect(Prime.list(5)).to eq([2, 3, 5])
      expect(Prime.list(10)).to eq([2, 3, 5, 7])
      expect(Prime.list(30)).to eq(primes)
    end
  end

  context '#first' do
    it 'should return empty array for 0' do
      expect(Prime.first(0)).to eq([])
    end

    it 'should list first 1 prime number' do
      expect(Prime.first(1)).to eq([2])
    end

    it 'should list first 10 prime numbers' do
      expect(Prime.first(10)).to eq(primes[0..9])
    end

    it 'should return an array in correct length' do
      expect(Prime.first(101).length).to eq(101)
    end
  end

end
