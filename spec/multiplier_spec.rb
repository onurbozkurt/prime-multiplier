RSpec.describe "Console", type: :cli do

  context 'table outputs' do

    it 'displays a table for 1' do
      fixture = File.read("spec/fixtures/table_1x1.txt")
      output = `ruby multiplier.rb 1`

      expect(output).to eq(fixture)
    end

    it 'displays a table for 5' do
      fixture = File.read("spec/fixtures/table_5x5.txt")
      output = `ruby multiplier.rb 5`

      expect(output).to eq(fixture)
    end

    it 'displays a table for 10' do
      fixture = File.read("spec/fixtures/table_10x10.txt")
      output = `ruby multiplier.rb 10`

      expect(output).to eq(fixture)
    end
  end

end
