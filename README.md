# Prime Multiplier

A small ruby application to calculate prime numbers and products of the primes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

```
Ruby 1.9.3 or greater
```

### Installing

Install the bundler gem

```
gem install bundler
```

Install project dependencies

```
bundle install
```

Optionally, you can give execute permission to multiplier.rb

```
chmod +x multiplier.rb
```

## Running the application

You can run the application with the number of primes you want. Default value is *10*

```
./multiplier.rb 20
```

or

```
ruby multiplier.rb 20
```

The applications sends the results to the STDOUT, so you can optionally write to a file

```
ruby multiplier.rb 100 > output.txt
```

## Running the tests

The projects uses RSpec for testing. To run all the tests, run the following command.

```
bundle exec rspec
```
